﻿using System.Collections.Generic;

namespace XMLParser.DbModel
{
    public class DbContext
    {
        public List<XmlUrl> XmlUrls { get; set; }
    }
}