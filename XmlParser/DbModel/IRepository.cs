﻿using System.Collections.Generic;

namespace XMLParser.DbModel
{
    public interface IRepository<T>
    {
        List<T> GetAll();   
        void Add(T entity);
        void Delete(int id);
        void SaveChanges(DbContext dbContext);

    }
}
