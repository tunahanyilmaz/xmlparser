﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using XMLParser.DbModel;

namespace XMLParser
{
    public partial class Form1 : Form
    {
        public static ReadXML ReadXML { get; set; }
        public static List<XDocument> XDocument { get; set; }
        public XmlUrlOperations XmlUrlOperations { get; set; }
      
        public Form1()
        {
            InitializeComponent();
            XDocument = new List<System.Xml.Linq.XDocument>();
            XmlUrlOperations = new XmlUrlOperations();          
            getUrlsForGrid();         
        }
        private void getUrlsForGrid()
        {
            dgv_UrlList.Rows.Clear();
            List<XmlUrl> XmlUrlsFromData = XmlUrlOperations.GetAllXmlUrls();
            foreach (var url in XmlUrlsFromData)
            {
                string[] newRow = new string[] {url.Name};
                dgv_UrlList.Rows.Add(newRow);
                DataGridViewCell cell_connect = dgv_UrlList.Rows[XmlUrlsFromData.IndexOf(url)].Cells[1];               
                cell_connect.Style.Padding = new Padding(10,3,10,3);
                DataGridViewCell cell_delete = dgv_UrlList.Rows[XmlUrlsFromData.IndexOf(url)].Cells[2];
                cell_delete.Style.Padding = new Padding(10, 3, 10, 3);
                DataGridViewCell cell_save = dgv_UrlList.Rows[XmlUrlsFromData.IndexOf(url)].Cells[3];
                cell_save.Style.Padding = new Padding(10, 3, 10, 3);
            }     
        }
        private void btnDefaultColor()
        {
            foreach (DataGridViewRow row in dgv_UrlList.Rows)
            {
                DataGridViewButtonCell cell = (DataGridViewButtonCell)row.Cells[1];
                cell.FlatStyle = FlatStyle.System;
                cell.Style.BackColor = Color.White;
            }
        }
        private void dgv_UrlList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                string filePath = dgv_UrlList.Rows[e.RowIndex == -1 ? 0 : e.RowIndex].Cells[(e.ColumnIndex - 1 == -1) ? e.ColumnIndex :e.ColumnIndex - 1 ].Value.ToString();
                ReadXML = new ReadXML(filePath);
                string[] filePathComp = filePath.Split('.', '\\', '/');
                DataGridViewButtonCell btn_connect = (DataGridViewButtonCell)dgv_UrlList.Rows[e.RowIndex == -1 ? 0 : e.RowIndex].Cells[1];

               if (filePathComp.Contains("www") && filePathComp.Contains("fulyan") && filePathComp.Contains("com") && filePathComp.Contains("tr"))
                {
                    
                    Task mainTask = Task.Factory.StartNew(() => PerformAction((int)nud_GroupProducts.Value, txt_PreperationValue.Text));
                    mainTask.Wait();
                    lbl_AccessLevel.Text = "Successfully Connected";
                    lbl_AccessLevel.ForeColor = Color.Green;
                    pnl_fulyan.Visible = true;
                    btnDefaultColor();
                    btn_connect.FlatStyle = FlatStyle.Flat;
                    btn_connect.Style.ForeColor = Color.Green;
                    btn_connect.Style.BackColor = Color.LightGreen;

                }
                else
                {
                    XDocument = new List<System.Xml.Linq.XDocument>();
                    Task mainTask = Task.Factory.StartNew(() => PerformActionForOthers());
                    mainTask.Wait();
                    pnl_fulyan.Visible = false;
                    if (XDocument != null)
                    {
                        lbl_AccessLevel.Text = "Successfully Connected";
                        lbl_AccessLevel.ForeColor = Color.Orange;
                        btnDefaultColor();
                        btn_connect.FlatStyle = FlatStyle.Flat;
                        btn_connect.Style.ForeColor = Color.Green;
                        btn_connect.Style.BackColor = Color.LightGreen;

                    }
                    else
                    {
                        lbl_AccessLevel.Text = "No Connection";
                        lbl_AccessLevel.ForeColor = Color.Red;
                        btnDefaultColor();

                    }


                }

            }
            if (e.ColumnIndex == 2)
            {
                dgv_UrlList.Rows.RemoveAt(e.RowIndex == -1 ? 0 : e.RowIndex);

                XmlUrlOperations.DeleteXmlUrl(e.RowIndex == -1 ? 0 : e.RowIndex);
            }
            if (e.ColumnIndex == 3)
            {
                XDocument = new List<System.Xml.Linq.XDocument>();
                string filePath = dgv_UrlList.Rows[e.RowIndex == -1 ? 0 : e.RowIndex].Cells[0].Value.ToString();
                ReadXML = new ReadXML(filePath);
                DataGridViewButtonCell btn_connect = (DataGridViewButtonCell)dgv_UrlList.Rows[e.RowIndex == -1 ? 0 : e.RowIndex].Cells[1];
                string[] filePathComp = filePath.Split('.', '\\', '/');
                if (filePathComp.Contains("www") && filePathComp.Contains("fulyan") && filePathComp.Contains("com") && filePathComp.Contains("tr"))
                {
                    
                    Task mainTask = Task.Factory.StartNew(() => PerformAction((int)nud_GroupProducts.Value, txt_PreperationValue.Text));
                    mainTask.Wait();
                    lbl_AccessLevel.Text = "Successfully Connected";
                    lbl_AccessLevel.ForeColor = Color.Green;
                    pnl_fulyan.Visible = true;
                    btnDefaultColor();
                    btn_connect.FlatStyle = FlatStyle.Flat;
                    btn_connect.Style.ForeColor = Color.Green;
                    btn_connect.Style.BackColor = Color.LightGreen;
                }
                else
                {

                    Task mainTask = Task.Factory.StartNew(() => PerformActionForOthers());
                    mainTask.Wait();
                    pnl_fulyan.Visible = false;
                   
                    if (XDocument != null)
                    {
                        lbl_AccessLevel.Text = "Successfully Connected";
                        lbl_AccessLevel.ForeColor = Color.Orange;
                        btnDefaultColor();
                        btn_connect.FlatStyle = FlatStyle.Flat;
                        btn_connect.Style.ForeColor = Color.Green;
                        btn_connect.Style.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        lbl_AccessLevel.Text = "No Connection";
                        lbl_AccessLevel.ForeColor = Color.Red;
                        btnDefaultColor();
                    }

                }
                SaveFileDialog path = new SaveFileDialog();
                path.Filter = "Xml FILE | *.xml";
                
                if (!(XDocument is null) && path.ShowDialog() == DialogResult.OK)
                {
                    ReadXML.Save(XDocument, (path.FileName), (int)nud_GroupProducts.Value);
                    MessageBox.Show("Saved");
                }
               
            }
        }
        private void btn_AddNewUrl_Click(object sender, EventArgs e)
        {
            XmlUrl newUrl = new XmlUrl();
            newUrl.Name = (txt_AddNewUrl.Text).Trim() ;
            if (newUrl.Name == "")
            {
                MessageBox.Show("Textbox is empty !");
            }
            else
            {
                XmlUrlOperations.AddXmlUrl(newUrl);
                getUrlsForGrid();
                txt_AddNewUrl.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XDocument = new List<System.Xml.Linq.XDocument>();
            lbl_AccessLevel.Text = "No Connection";
            lbl_AccessLevel.ForeColor = Color.Red;
            btnDefaultColor();
        }

        private void btn_SaveAsCsvFile_Click(object sender, EventArgs e)
        {
                Task mainTask = Task.Factory.StartNew(() => PerformActionConvertToCSV());
            mainTask.Wait();
            MessageBox.Show("Saved Path : C:Data\\");
        }
    }
}
