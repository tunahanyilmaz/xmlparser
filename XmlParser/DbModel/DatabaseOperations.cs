﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XMLParser.DbModel
{
    public class DatabaseOperations
    {
        public static DbContext DbCon { get; set; }

        //public const string XmlUrlPath = "C:\\Users\\TYılmaz\\Desktop\\XmlUrls.txt";
        public const string XmlUrlPath = "C:\\Data\\XmlUrls.txt";
        static DatabaseOperations()
        {
            DbCon = ReadAllDataFromFile();
        }

        public static DbContext ReadAllDataFromFile()
        {
            DbContext dbCon = new DbContext();
            if (!Directory.Exists("C:\\Data"))
            {
                Directory.CreateDirectory("C:\\Data");
            }
            if (File.Exists(XmlUrlPath))
            {
                string readedData = File.ReadAllText(XmlUrlPath);
                dbCon.XmlUrls = JsonConvert.DeserializeObject<List<XmlUrl>>(readedData);
            }
            else
            {
                dbCon.XmlUrls = new List<XmlUrl>();
            }
            return dbCon;
        }
        public static void WriteAllDataToFile(DbContext dbCon)
        {
            try
            {
                File.WriteAllText(XmlUrlPath, JsonConvert.SerializeObject(dbCon.XmlUrls));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
