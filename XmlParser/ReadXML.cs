﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XMLParser
{
    public class ReadXML
    {
        public string FilePath { get; set; }

        public ReadXML(string filePath)
        {
            this.FilePath = filePath;
        }
        public async Task<bool> ControlFileAsync()
        {
            return await Task.FromResult(File.Exists(FilePath));
        }
        public async Task<XDocument> ReadXDocumentAsync()
        {
            try
            {
                return await Task.FromResult(XDocument.Load(FilePath));
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Invalid URL");
                throw;
            }
            
        }
        public async Task<List<XElement>> GetAllSpecificNodesAsyc(XDocument xDocument, string nodeName)
        {
            return await Task.FromResult(xDocument.Descendants(nodeName).ToList());
        }
        public async Task<bool> ControlSpecificNodeInXElement(XElement element, string nodeName)
        {
            return await Task.FromResult(element.Descendants(nodeName).ToList().Count != 0);
        }
        public void Save(List<XDocument> xDocument, string filePath, int divideNumber)
        {
            int tempDivideNumber = divideNumber;
            string[] fileName = filePath.Split('.');
            if (xDocument.Count != 1)
            {  
                foreach (var item in xDocument)
                {
                    item.Save(fileName[0] + "-" + (xDocument.IndexOf(item)).ToString().PadLeft(3, '0') + tempDivideNumber + '.' + fileName[1]);
                    tempDivideNumber += divideNumber;
                }
            }
            else
            {
                xDocument[0].Save(filePath);
            }
        }
    }
}
