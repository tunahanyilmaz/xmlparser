﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XMLParser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_AccessLevel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnl_fulyan = new System.Windows.Forms.Panel();
            this.dgv_UrlList = new System.Windows.Forms.DataGridView();
            this.Column_Url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Connect = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column_Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column_Save = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_AddNewUrl = new System.Windows.Forms.TextBox();
            this.btn_AddNewUrl = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nud_GroupProducts = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_PreperationValue = new System.Windows.Forms.TextBox();
            this.btn_SaveAsCsvFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_UrlList)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_GroupProducts)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_AccessLevel
            // 
            this.lbl_AccessLevel.AutoSize = true;
            this.lbl_AccessLevel.ForeColor = System.Drawing.Color.Red;
            this.lbl_AccessLevel.Location = new System.Drawing.Point(731, 291);
            this.lbl_AccessLevel.Name = "lbl_AccessLevel";
            this.lbl_AccessLevel.Size = new System.Drawing.Size(78, 13);
            this.lbl_AccessLevel.TabIndex = 3;
            this.lbl_AccessLevel.Text = "No Connection";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(687, 291);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Status :";
            // 
            // pnl_fulyan
            // 
            this.pnl_fulyan.Location = new System.Drawing.Point(683, 28);
            this.pnl_fulyan.Name = "pnl_fulyan";
            this.pnl_fulyan.Size = new System.Drawing.Size(169, 252);
            this.pnl_fulyan.TabIndex = 5;
            this.pnl_fulyan.Visible = false;
            // 
            // dgv_UrlList
            // 
            this.dgv_UrlList.AllowUserToAddRows = false;
            this.dgv_UrlList.AllowUserToDeleteRows = false;
            this.dgv_UrlList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_UrlList.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_UrlList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_UrlList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_UrlList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_Url,
            this.Column_Connect,
            this.Column_Delete,
            this.Column_Save});
            this.dgv_UrlList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_UrlList.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgv_UrlList.Location = new System.Drawing.Point(0, 0);
            this.dgv_UrlList.Name = "dgv_UrlList";
            this.dgv_UrlList.Size = new System.Drawing.Size(528, 225);
            this.dgv_UrlList.TabIndex = 6;
            this.dgv_UrlList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_UrlList_CellContentClick);
            // 
            // Column_Url
            // 
            this.Column_Url.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column_Url.DividerWidth = 5;
            this.Column_Url.HeaderText = "URL";
            this.Column_Url.Name = "Column_Url";
            this.Column_Url.ReadOnly = true;
            this.Column_Url.Width = 337;
            // 
            // Column_Connect
            // 
            this.Column_Connect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column_Connect.DividerWidth = 5;
            this.Column_Connect.FillWeight = 25F;
            this.Column_Connect.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Column_Connect.HeaderText = "Connect";
            this.Column_Connect.Name = "Column_Connect";
            this.Column_Connect.Width = 55;
            // 
            // Column_Delete
            // 
            this.Column_Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column_Delete.DividerWidth = 5;
            this.Column_Delete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Column_Delete.HeaderText = "Delete";
            this.Column_Delete.Name = "Column_Delete";
            this.Column_Delete.Width = 49;
            // 
            // Column_Save
            // 
            this.Column_Save.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column_Save.DividerWidth = 5;
            this.Column_Save.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Column_Save.HeaderText = "Save";
            this.Column_Save.MinimumWidth = 2;
            this.Column_Save.Name = "Column_Save";
            this.Column_Save.Width = 43;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgv_UrlList);
            this.panel1.Location = new System.Drawing.Point(3, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(528, 225);
            this.panel1.TabIndex = 7;
            // 
            // txt_AddNewUrl
            // 
            this.txt_AddNewUrl.Location = new System.Drawing.Point(12, 260);
            this.txt_AddNewUrl.Name = "txt_AddNewUrl";
            this.txt_AddNewUrl.Size = new System.Drawing.Size(383, 20);
            this.txt_AddNewUrl.TabIndex = 8;
            // 
            // btn_AddNewUrl
            // 
            this.btn_AddNewUrl.Location = new System.Drawing.Point(401, 258);
            this.btn_AddNewUrl.Name = "btn_AddNewUrl";
            this.btn_AddNewUrl.Size = new System.Drawing.Size(130, 23);
            this.btn_AddNewUrl.TabIndex = 9;
            this.btn_AddNewUrl.Text = "Add New URL";
            this.btn_AddNewUrl.UseVisualStyleBackColor = true;
            this.btn_AddNewUrl.Click += new System.EventHandler(this.btn_AddNewUrl_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(606, 286);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Disconnect";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(539, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Group products";
            // 
            // nud_GroupProducts
            // 
            this.nud_GroupProducts.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nud_GroupProducts.Location = new System.Drawing.Point(537, 31);
            this.nud_GroupProducts.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nud_GroupProducts.Name = "nud_GroupProducts";
            this.nud_GroupProducts.Size = new System.Drawing.Size(82, 20);
            this.nud_GroupProducts.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(534, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Preparation Date";
            // 
            // txt_PreperationValue
            // 
            this.txt_PreperationValue.Location = new System.Drawing.Point(534, 86);
            this.txt_PreperationValue.Name = "txt_PreperationValue";
            this.txt_PreperationValue.Size = new System.Drawing.Size(100, 20);
            this.txt_PreperationValue.TabIndex = 15;
            // 
            // btn_SaveAsCsvFile
            // 
            this.btn_SaveAsCsvFile.Location = new System.Drawing.Point(534, 125);
            this.btn_SaveAsCsvFile.Name = "btn_SaveAsCsvFile";
            this.btn_SaveAsCsvFile.Size = new System.Drawing.Size(100, 23);
            this.btn_SaveAsCsvFile.TabIndex = 16;
            this.btn_SaveAsCsvFile.Text = "Save as CSV File";
            this.btn_SaveAsCsvFile.UseVisualStyleBackColor = true;
            this.btn_SaveAsCsvFile.Click += new System.EventHandler(this.btn_SaveAsCsvFile_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(864, 315);
            this.Controls.Add(this.btn_SaveAsCsvFile);
            this.Controls.Add(this.txt_PreperationValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nud_GroupProducts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_AddNewUrl);
            this.Controls.Add(this.txt_AddNewUrl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl_fulyan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_AccessLevel);
            this.MaximumSize = new System.Drawing.Size(880, 354);
            this.MinimumSize = new System.Drawing.Size(880, 354);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_UrlList)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nud_GroupProducts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private async static Task PerformAction(int divideNumber, string preperation_value)
        {
            XDocument allXDocuments = await ReadXML.ReadXDocumentAsync();
            List<XElement> AllProducts = await ReadXML.GetAllSpecificNodesAsyc(allXDocuments, "product");
            XDocument temp = new System.Xml.Linq.XDocument();
            temp.Add(new XElement("products"));
            int tempIndex = divideNumber;
            divideNumber--;
            if (divideNumber < 50)
            {
                divideNumber = AllProducts.Count - 1;
            }
            foreach (XElement prod in AllProducts)
                {
                    
                    double price_list = Double.Parse(prod.Element("price_list").Value, new CultureInfo("en-US"));
                    double vat = Double.Parse(prod.Element("vat").Value.Replace('.', ','), new CultureInfo("en-US"));
                    double price_list_vat_included = price_list * (1 + vat / 100);
                    bool price_list_vat_includedExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "price_list_vat_included");
                    if (price_list_vat_includedExistance == false)
                    {
                        XElement price_list_vat_included_element = new XElement("price_list_vat_included", price_list_vat_included);
                        prod.Add(price_list_vat_included_element);
                    }
                
                    double price_special_vat_included = Double.Parse(prod.Element("price_special_vat_included").Value, new CultureInfo("en-US"));
                    double price_special_rate = Convert.ToDouble(prod.Element("price_special_rate").Value.Replace('.', ','));
                    var cat1nameNodes = prod.Element("cat1name");
                    var cat1codeNodes = prod.Element("cat1code");
                    string category_path_try = null;
                    category_path_try = prod.Element("category_path").Value.Replace("FULYAN > ", "");         
                if (category_path_try != null)
                 {
                    prod.Element("category_path").Value = category_path_try;
                }
                if (cat1codeNodes != null)
                    {
                        cat1codeNodes.Remove();
                    }
                    if (cat1nameNodes != null)
                    {
                        cat1nameNodes.Remove();
                    }
                XElement price_special_vat_included_element = new XElement("price_special_vat_included", 0);
                    prod.Add(price_special_vat_included_element);
                    
                    double discount_price = price_list * (1 + vat / 100) - price_special_vat_included;
                    discount_price = (discount_price < 0) ? 0 : discount_price;

                    XElement discount_price_element = new XElement("discount_price", discount_price);

                    bool discount_typeExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "discount_type");
                    bool preperationExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "preperation");
                    if (preperationExistance == false)
                    {
                        XElement preperation_element = new XElement("preperation", preperation_value == "" ? "1" : preperation_value);
                        prod.Add(preperation_element);
                    }
                    if (discount_typeExistance == false)
                    {
                        XElement discount_type = new XElement("discount_type", (price_special_rate == 0 ? 0 : 1.00));
                        prod.Add(discount_type);
                    }
                    bool subProdExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "subproducts");
                    bool discount_priceExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "discount_price");
                    if (discount_priceExistance == false)
                    {
                        prod.Add(discount_price_element);
                    }
                    if (subProdExistance == false)
                    {
                        XElement subProducts = new XElement("subproducts");
                        XElement subProduct = new XElement("subproduct");
                        XElement stockElement = new XElement("stock", prod.Element("stock").Value);
                        XElement codeElement = new XElement("code", string.Concat(prod.Element("code").Value, "1"));
                        XElement vatElement = new XElement("vat", prod.Element("vat").Value);
                        XElement priceListElement = new XElement("price_list", Convert.ToDouble(prod.Element("price_list").Value.Replace('.', ',')));
                        XElement varientGroupIdElement = new XElement("VaryantGroupID", prod.Element("code").Value);
                        XElement type1Element = new XElement("type1", "Standart");
                        subProduct.Add(codeElement);
                        subProduct.Add(varientGroupIdElement);
                        subProduct.Add(stockElement);
                        subProduct.Add(type1Element);
                        subProduct.Add(priceListElement);
                        subProduct.Add(vatElement);
                        subProducts.Add(subProduct);
                        prod.Add(subProducts);
                    }

                    foreach (var subProd in prod.Descendants("subproduct"))
                    {

                        subProd.Element("type1").SetAttributeValue("name", "Beden");
                        if (subProd.Element("type1").Value == "<![CDATA[ Standart  ]]> ")
                        {
                            subProd.Element("type1").SetValue("Standart");
                        }
                        bool discount_price_subExistance = await ReadXML.ControlSpecificNodeInXElement(subProd, "discount_price");
                        bool subproduct_priceExistance = await ReadXML.ControlSpecificNodeInXElement(subProd, "subproduct_price");
                        if (subproduct_priceExistance == false)
                        {
                            XElement subproduct_price_element = new XElement("subproduct_price", 0);
                            subProd.Add(subproduct_price_element);
                        }
                        if (discount_price_subExistance == false)
                        {
                            subProd.Add(discount_price_element);
                        }
                    }
                    if (AllProducts.IndexOf(prod) != divideNumber && AllProducts.Count - 1 != AllProducts.IndexOf(prod))
                    {
                        temp.Element("products").Add(prod);
                    }
                    else
                    {
                        temp.Element("products").Add(prod);
                        XDocument.Add(temp);
                        temp = new System.Xml.Linq.XDocument();
                        temp.Add(new XElement("products"));
                        divideNumber += tempIndex;
                    }
                }
            
            
        }
        private async static Task PerformActionForOthers()
        {
            XDocument allXDocuments = await ReadXML.ReadXDocumentAsync();
            XDocument.Add(allXDocuments);

        }
        private async static Task PerformActionConvertToCSV()
        {
            XDocument allXDocuments = await ReadXML.ReadXDocumentAsync();
            XDocument.Add(allXDocuments);
            List<XElement> AllProducts = await ReadXML.GetAllSpecificNodesAsyc(allXDocuments, "product");

            var csv = new StringBuilder();
            csv.AppendLine(";cat2name;cat3name;cat4name");
            foreach (XElement prod in AllProducts)
            {
                string first = "";
                bool firstExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "cat2name");
                if (firstExistance == true)
                {
                    first = prod.Element("cat2name").Value.ToString();
                }
                string second = "";
                bool secondExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "cat3name");
                if (secondExistance == true)
                {
                    second = prod.Element("cat3name").Value.ToString();
                }
                string third = "";
                bool thirdExistance = await ReadXML.ControlSpecificNodeInXElement(prod, "cat4name");
                if (thirdExistance == true)
                {
                    third = prod.Element("cat4name").Value.ToString();
                }
                var newLine = string.Format("Product{0};{1};{2};{3}", AllProducts.IndexOf(prod), first, second, third);
                csv.AppendLine(newLine);
            }
            File.WriteAllText("C:\\Data\\products.csv", csv.ToString());
        }
        #endregion
        private System.Windows.Forms.Label lbl_AccessLevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnl_fulyan;
        private System.Windows.Forms.DataGridView dgv_UrlList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt_AddNewUrl;
        private System.Windows.Forms.Button btn_AddNewUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Url;
        private System.Windows.Forms.DataGridViewButtonColumn Column_Connect;
        private System.Windows.Forms.DataGridViewButtonColumn Column_Delete;
        private System.Windows.Forms.DataGridViewButtonColumn Column_Save;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nud_GroupProducts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_PreperationValue;
        private System.Windows.Forms.Button btn_SaveAsCsvFile;
    }
}

