﻿using System.Collections.Generic;

namespace XMLParser.DbModel
{
    class XmlUrlRepository : IRepository<XmlUrl>
    {
        public List<XmlUrl> XmlUrls { get; set; }
        public XmlUrlRepository(List<XmlUrl> xmlUrl)
        {
            this.XmlUrls = xmlUrl;
        }

        public void Add(XmlUrl entity)
        {
            XmlUrls.Add(entity);
        }
        public void Delete(int id)
        {
            try
            {
                XmlUrls.RemoveAt(id);
            }
            catch (System.Exception)
            {
                System.Windows.Forms.MessageBox.Show("Error While Deleting Current Url [No Id Found]");
                throw;
            }
            
        }
        public void Delete(XmlUrl xmlUrl)
        {
            XmlUrls.Remove(xmlUrl);
        }
        public List<XmlUrl> GetAll()
        {
            return XmlUrls;
        }
        public void SaveChanges(DbContext dbContext)
        {
            DatabaseOperations.WriteAllDataToFile(dbContext);
        }
    }
}
