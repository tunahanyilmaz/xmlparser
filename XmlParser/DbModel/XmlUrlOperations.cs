﻿using System.Collections.Generic;
using System.Linq;

namespace XMLParser.DbModel
{
    public class XmlUrlOperations
    {
        private DbContext dbContext { get; set; }
        private XmlUrlRepository XmlUrlRepository { get; set; }
        public XmlUrlOperations()
        {
            dbContext = DatabaseOperations.DbCon;
            XmlUrlRepository = new XmlUrlRepository(dbContext.XmlUrls);
        }

        public List<XmlUrl> GetAllXmlUrls()
        {
            return XmlUrlRepository.GetAll().OrderBy(Order => Order.Id).ToList();
        }
        public void DeleteXmlUrl(int id)
        {
            XmlUrlRepository.Delete(id);
            XmlUrlRepository.SaveChanges(dbContext);
        }
        public void DeleteXmlUrl(XmlUrl xmlUrl)
        {
            XmlUrlRepository.Delete(xmlUrl);
            XmlUrlRepository.SaveChanges(dbContext);
        }

        public void AddXmlUrl(XmlUrl xmlUrl)
        {
            XmlUrlRepository.Add(xmlUrl);
            XmlUrlRepository.SaveChanges(dbContext);
        }
    }
}
